//
//  TableViewController.swift
//  calculadora
//
//  Created by Miguel Montalvillo on 11/8/17.
//  Copyright © 2017 Miguel Montalvillo. All rights reserved.
//

import UIKit
import CoreData


class TableViewController: UITableViewController {
    
    var managedObjects:[NSManagedObject] = []
    
    @IBOutlet var tableV_lista: UITableView!
    

    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 0
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return managedObjects.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        //let celda = UITableViewCell()
        let celda = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        
        let managedObject = managedObjects[indexPath.row]
        
        celda.textLabel?.text = managedObject.value(forKey: "operacion") as? String
        
        return celda
    }
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
