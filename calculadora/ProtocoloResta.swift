//
//  ProtocoloResta.swift
//  calculadora
//
//  Created by Miguel Montalvillo on 11/8/17.
//  Copyright © 2017 Miguel Montalvillo. All rights reserved.
//

import UIKit

protocol ProtocoloResta {
    func resta(valor1:Int, valor2:Int) -> Int
}

extension ProtocoloResta {
    func resta(valor1:Int, valor2:Int) -> Int {
        return valor1 - valor2
    }
}
