//
//  ViewController.swift
//  calculadora
//
//  Created by Miguel Montalvillo on 11/8/17.
//  Copyright © 2017 Miguel Montalvillo. All rights reserved.
//

import UIKit


class ViewController: UIViewController, ProtocoloSuma, ProtocoloResta {

    @IBOutlet weak var valor1: UITextField!
    @IBOutlet weak var valor2: UITextField!
    @IBOutlet weak var valorResultado: UILabel!
    
    @IBAction func sumar(_ sender: UIButton) {
        let a = Int(valor1.text!)
        let b = Int(valor2.text!)
        
        valorResultado.text = String(suma(a:a!, b:b!))
    }
    
    @IBAction func restar(_ sender: UIButton) {
        let a = Int(valor1.text!)
        let b = Int(valor2.text!)
        
        valorResultado.text = String(resta(valor1:a!, valor2:b!))
    }
    
    @IBAction func multiplicar(_ sender: Any) {
    }
    
    @IBAction func dividir(_ sender: UIButton) {
    }
    
    @IBAction func btn_segue(_ sender: UIBarButtonItem) {
        //self.performSegue(withIdentifier: "segue_lista", sender: <#T##Any?#>)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        //let objLista:TableViewController = segue.destination as! TableViewController
    }


}

