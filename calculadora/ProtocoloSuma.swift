//
//  ProtocoloSuma.swift
//  calculadora
//
//  Created by Miguel Montalvillo on 11/8/17.
//  Copyright © 2017 Miguel Montalvillo. All rights reserved.
//

import UIKit

protocol ProtocoloSuma {
    func suma(a:Int, b:Int) -> Int
}

extension ProtocoloSuma {
    func suma(a:Int, b:Int) -> Int {
        return a + b
    }
}
